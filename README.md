# IMC LME Kafka Driver Developer Documentation

Table of Contents
=================
* [Market Contacts (Support)](#market-contacts)
* [Market Documentation (Support)](#market-documentation)
* [Test Access](#test-access)
* [Generate POJO from avro schema](#generate-pojo-from-avro-schema)
* [Dictionary](#dictionary)
* [Frequently Asked Questions](#frequently-asked-questions)

## Market Contacts





## Market Documentation


Market documentation can be found under /space/common/doc/finance/LME Kafka/


## Tests Access



## Generate POJO from avro schema

The latest avro schema should be available [here](imc-lme-kafka/doc/schema/schema-dev.tradableinstrument-value-v6_latest_version_from_ESP.json).
To generate Java objects from that schema, perform the following steps:

- Download the latest avro-tools jar from the [official site](https://avro.apache.org/releases.html)
- Check that the `namespace` in the schema file is `com.ingalys.imc.lme.kafka.avro` meaning we want to generate POJOs under this package. This path is relative to the location where the `compile command will be run`.
- Change directory to `src/main/java`
- Run `java -jar /path/to/avro-tools-1.10.0.jar compile schema <schema file> .` 
    
Do note the `dot (.)`at the end of the command. This should generate all required `Java objects`. 
You may encounter some erros (different naming convention, new fields, etc.) in the code if the schema has changed in the meantime.
You will then need to refactor the code.
    


## Dictionary
Dictionary is populated from messages coming from a Kafka cluster.
What is particular to this project is the fact that messages will be published inside an array in one single message. 


## Frequently Asked Questions

